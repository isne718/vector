//620615018
//620615024
#include<iostream>
#include<vector>
#include<cstdlib>
#include<ctime>
using namespace std;

void sumOfnum(vector<int>number,int rancount){ //sum of all elements //Big o = n
	int sum;

	for (int i=0;i<rancount;i++){
		sum = sum+number[i];
	}
	cout << "\nSum of all elements : "<<sum <<endl;
}
int highestValue(vector<int>number,int rancount){//highest //Big o = n
	int highest=number[0];
	for (int i=0;i<rancount;i++){
		if(number[i]>highest){ //if number more than highest
			highest=number[i]; //give highest=number[i]
		}
	}
	cout << "Highest : "<<highest<<endl;
}

int lowestValue(vector<int>number,int rancount){//lowest //Big o = n
	int lowest=number[0];
	for (int i=0;i<rancount;i++){
		if(number[i]<lowest){
			lowest=number[i];
		}
	}
	cout << "Lowest : "<<lowest<<endl;
}

int mean(vector<int>number,int rancount){//mean //Big o = n
	int mean;
	int sum;
	for (int i=0;i<rancount;i++){
		sum = sum+number[i];
	}
	mean=sum/rancount;
	cout << "Mean : "<<mean<<endl;
}

int median(vector<int>number,int rancount){//median //Big o = constant
	int median;
	if(rancount%2!=0){//odd size
		median=number[(rancount+1)/2];
	}else{
		median=(number[(rancount+1)/2]+number[(rancount+1)/2-1])/2;//even size //Big o = n
	}
	cout << "Median : "<<median<<endl;
}

int mode(vector<int>number,int rancount){//mode //Big o = n^2
	int mode, round=0, counter=0, max=0;
	for(int i=0;i<rancount;i++){
		counter=number[i];
		for(int j=0;j<rancount;j++){
			if(counter==number[j]){
				round++;
				counter=number[j];
			}
			
			if(round>max){
				max=round;
				mode=number[j];
			}
		}
		round=0;
	}
	cout << "Mode : "<<mode<<endl;
}

 
int range(vector<int>number,int rancount){//range //Big o = 2n
	int range;
	int highest=number[0];
	for (int i=0;i<rancount;i++){
		if(number[i]>highest){
			highest=number[i];
		}
	}
	int lowest=number[0];
	for (int i=0;i<rancount;i++){
		if(number[i]<lowest){
			lowest=number[i];
		}
	}
	range = highest - lowest;
	cout << "Range : " << range << endl;
}


int evennumber(vector<int>number,int rancount){//even //Big o = n
	int evennum;
	for (int i=0;i<rancount;i++){
		if(number[i]%2==0){
			evennum = evennum + 1;
		}
	}
	cout << "Even Num : "<<evennum<<endl;
} 

int oddnumber(vector<int>number,int rancount){//odd //Big o = n
	int oddnum;
	for (int i=0;i<rancount;i++){
		if(number[i]%2!=0){
			oddnum = oddnum + 1;
		}
	}
	cout << "Odd Num : "<<oddnum<<endl;
} 

void bubbleSort(vector<int>&number,int rancount){//bubble sort //Big o = n^2
	int hold;
	int swap;
	do{
		swap=0;
		for(int i=0; i<rancount;i++){
			if(number[i]>number[i+1]){
			hold=number[i];
			number[i]=number[i+1];
			number[i+1]=hold;
			swap=1;
		}
		}
	
	}while (swap==1);
	
}

void lowTohigh(vector<int>number,int rancount){//low to high //Big o = n^2
	int hold;
	int swap;
	do{
		swap=0;
		for(int i=0; i<rancount;i++){
			if(number[i]>number[i+1]){
			hold=number[i];
			number[i]=number[i+1];
			number[i+1]=hold;
			swap=1;
		}
		}
	
	}while (swap==1);
	cout << "Lowest to Highest : " << endl;
	for (int i=0;i<rancount;i++){
		cout << number[i] <<" "; 
		if(i%20==0&&i!=0){
			cout << endl;
		}
	}
	cout <<endl;
}

int main(){
srand(time(0));
	
	vector<int>number;
	int rancount = 50+rand()%101; //size of elememt
	for (int i=0;i<rancount;i++){
		int x = rand()%101; //element
		number.push_back(x);
	}
	cout << "The number of an elements : " << endl;
	for (int i=0;i<rancount;i++){
		cout << number[i] <<" "; 
		if(i%20==0&&i!=0){
			cout << endl;
		}
	}
	
	sumOfnum(number, rancount);
	highestValue(number, rancount);
	lowestValue(number, rancount);	
	mean(number, rancount);
	bubbleSort(number, rancount);
	median(number, rancount);
	mode(number, rancount);
	range(number, rancount);
	evennumber(number, rancount);
	oddnumber(number, rancount);
	lowTohigh(number, rancount);
	
	
	return 0;
}
